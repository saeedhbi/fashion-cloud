const { createContainer, asClass, asFunction, asValue } = require('awilix');
const { scopePerRequest } = require('awilix-express');
const Database = require('./server/database');

const config = require('./config');
const App = require('./app/index');

const Server = require('./server');
const router = require('./server/router');

const contractController = require('@domain/contract/controllers');

const contractService = require('@domain/contract/services/contractService');

const contractRepository = require('@domain/contract/dataAccess/repositories/contractRepository');

const db = new Database(config.database);

const container = createContainer();

container
.register({
  app: asClass(App).singleton(),
  server: asClass(Server).singleton()
})
.register({
  contractController: asClass(contractController)
})
.register({
  contractService: asClass(contractService),
})
.register({
  contractRepository: asClass(contractRepository).singleton(),
})
.register({
  router: asFunction(router).singleton(),
  database: asFunction(() => db).singleton()
})
.register({
  containerMiddleware: asValue(scopePerRequest(container)),
  config: asValue(config)
});

module.exports = container;