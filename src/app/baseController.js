class BaseController {
	success(res, data) {
		return res.json({
			data
		}) 
	}

	error(res, {status, message}) {
		status = status || 500;

		return res.status(status).json({
			errors: {
				status, message
			}
		})
	}
}

module.exports = BaseController;