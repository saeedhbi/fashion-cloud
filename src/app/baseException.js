class BaseException {
	constructor() {
		this.status = 500;
		this.message = 'There is a problem';
	}
}

module.exports = BaseException;