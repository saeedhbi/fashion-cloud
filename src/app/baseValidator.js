const { validationResult } = require('express-validator');

class BaseValidator {
	static validate(validations) {
		return async (req, res, next) => {
			await Promise.all(validations.map(validation => validation.run(req)));

			const errorFormatter = ({ msg, param }) => {
				return `${param}: ${msg}`;
			};
			const errors = validationResult(req).formatWith(errorFormatter);


			if (errors.isEmpty()) {
				return next();
			}

			res.status(422).json({ errors: errors.array() });
		};
	}

	static isNumber(value) {
		return isFinite(value);
	}

	static isString(value) {
		return (typeof value === "string");
	}

	static isBoolean(value) {
		return (typeof value === "boolean");
	}
}

module.exports = BaseValidator;