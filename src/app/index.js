class App {
  constructor({ server, database }) {
    this.server = server;
    this.database = database;
  }

  async start() {
    await this.database.connect();

    await this.server.start();
  }
}

module.exports = App;