module.exports = {
  web: {
  	port: process.env.APP_POST || 5000
  },
  database: {
    user: process.env.DB_USER || "default",
    password: process.env.DB_PASSWORD || "123",
    database: process.env.DB_NAME || "default",
    host: process.env.DB_HOST || "127.0.0.1",
    port: process.env.DB_PORT || 27017
  },
  app: {
    limit: process.env.APP_CACHE_LIMIT || 2
  }
};