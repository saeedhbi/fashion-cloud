const fs = require('fs');
const path = require('path');

const ENV = process.env.NODE_ENV || 'app';

const config = require(path.join(__dirname, 'env', ENV));

module.exports = config;