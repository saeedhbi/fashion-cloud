const BaseController = require('@app/baseController');

class ContractController extends BaseController {
	constructor( { contractService } ) {
		super();
		this.contractService = contractService;
	}

	async getContract(res, req, next) {
		try {
			let data = await this.contractService.getContract(req.params.id);

			return this.success(res, data);
		} catch (e) {
			return this.error(res, e);
		}
	}

	async getAllContracts(res, req, next) {
		try {
			let data = await this.contractService.getAll();

			return this.success(res, data);
		} catch (e) {
			return this.error(res, e);
		}
	}

	async postUpdateContract(res, req, next) {
			try {
				let data = await this.contractService.updateContract(req.body.id, req.body.data);

				return this.success(res, data);
			} catch (e) {
				return this.error(res, e);
			}
	}

	async deleteContract(res, req, next) {
		try {
			let data = await this.contractService.deleteContract(req.params.id);

			return this.success(res, data);
		} catch (e) {
			return this.error(res, e);
		}
	}

	async deleteAll(res, req, next) {
		try {
			let data = await this.contractService.deleteAll();

			return this.success(res, data);
		} catch (e) {
			return this.error(res, e);
		}
	}
}

module.exports = ContractController;