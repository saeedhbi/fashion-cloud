let mongoose = require('mongoose')

let contractSchema = new mongoose.Schema({
	id: {
		type: String,
		required: true,
		unique: true
	},
	data: String,
	created_at: {
		type: Date,
		index: { expires: '2m' }
	}
})

module.exports = mongoose.model('Contract', contractSchema)