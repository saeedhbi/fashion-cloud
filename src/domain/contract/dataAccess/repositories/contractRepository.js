const Contract = require('@domain/contract/dataAccess/entities/contract');
const NotFoundException = require('@domain/contract/exceptions/notFoundException');

class ContractRepository {
	constructor({ database }) {
		this.database = database;
		
		this.table = 'contracts';
	}

	async createContract(id, data) {
		let contract = new Contract({
			id,
			data,
			created_at: Date.now()
		});

		return await contract.save();
	}	

	async findById(contractId) {
		let data = await Contract.findOneAndUpdate({
			id: contractId
		}, {
			created_at: Date.now()
		});

		if (!data) throw new NotFoundException;

		return data;
	}

	async all() {
		// If there is big data here, we can use cursor() option to retrieve data chunkly. 
		return await Contract.find();
	}

	async updateById(id, data) {
		console.log(id);
		let result = await Contract.findOneAndUpdate({
			id
		}, {
			data,
			created_at: Date.now()
		}, {
			new: true
		});

		if (!result) throw new NotFoundException;

		return result;
	}

	async updateOldestContract(id, data) {
		// There are two option to override oldest entry
		// 1. Remove it and create new 
		// 2. Create new attribute in object as key of ID to change it manual. MongoDB doesn't allow changing default ID.


		return await Contract.findOneAndUpdate({}, {
			id,
			data,
			created_at: Date.now()
		}, {
			sort: {
				'created_at': 1
			}
		});
	}

	async deleteById(id) {
		return await Contract.remove({id});
	}

	async deleteAll() {
		return await Contract.remove();
	}

	async countContracts() {
		return await Contract.countDocuments();
	}
}
	
module.exports = ContractRepository;