const BaseNotFoundException = require('@exceptions/notFoundException');

class NotFoundException extends BaseNotFoundException {
	constructor() {
		super();

		this.message = 'Contract Not Found';
	}
}

module.exports = NotFoundException;