const { Router } = require('express');
const ContractValidator = require('@domain/contract/validators');

class ContractsAPI {

	constructor(contractController) {

		this.controller = contractController;

		return this;
	}

	router() {
		const router = new Router();

		router.get('/all',
			async (req, res, next) => {
				return await this.controller.getAllContracts(res, req, next);
			}
		);

		router.get('/:id',
			ContractValidator.validate(ContractValidator.contractId()),
			async (req, res, next) => {
				return await this.controller.getContract(res, req, next);
			}
		);

		router.post('/',
			ContractValidator.validate(ContractValidator.contractUpdate()),
			async (req, res, next) => {
				return await this.controller.postUpdateContract(res, req, next);
			}
		);

		router.delete('/:id',
			ContractValidator.validate(ContractValidator.contractId()),
			async (req, res, next) => {
				return await this.controller.deleteContract(res, req, next);
			}
		);

		router.delete('/',
			async (req, res, next) => {
				return await this.controller.deleteAll(res, req, next);
			}
		);

		return router;
	}
}

module.exports = ContractsAPI;