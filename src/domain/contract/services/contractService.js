class ContractService {
	constructor ({config, contractRepository}) {
		this.config = config;
		this.contractRepository = contractRepository;
	}

	async getContract(id) {
		let data = null;

		try {
			data = await this.contractRepository.findById(id);

			console.log('Cache hit');
		} catch(e) {
			console.log('Cache miss');

			data = this._createRandomString();

			// Default TTL time checking by mongo is 60s. So all entries by behind expire time will be expired at same.
			// I set 2m for index expire time. Check Contract entry file.

			if (await this._checkCacheCanCreate()) {
				await this.contractRepository.createContract(id, data);
			} else {
				// I wanted to use capped collections to only record a fixed amount of documents (as required by the task) however one requirement which caused a conflict with capped collections was having the ability to delete a single document. 
				// When you use a capped collection you cannot delete a single document. Therefore, capped collections are not used.

				// TTL indexes cannot be used with capped collections, since we are not using capped collections, we use a TTL index to expire old documents, automatically by MongoDB.
				await this.contractRepository.updateOldestContract(id, data);
			}
		} 
		
		return data;
	}

	async getAll() {
		return await this.contractRepository.all();
	}

	async updateContract(contractId, data) {
			return await this.contractRepository.updateById(contractId, data);
	}

	async deleteContract(id) {
		return await this.contractRepository.deleteById(id);
	}

	async deleteAll() {
		return await this.contractRepository.deleteAll();
	}

	async _checkCacheCanCreate() {
		 let count = await this.contractRepository.countContracts();

		 return this.config.app.limit > count;
	}

	_createRandomString() {
		const c = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

		return [...Array(20)].map(_ => c[~~(Math.random() * c.length)]).join('')
	}
}

module.exports = ContractService