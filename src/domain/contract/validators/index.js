const { check } = require('express-validator');
const BaseValidator = require('@app/baseValidator');

class ContractValidator extends BaseValidator {

  	static contractId() {
  	  return [
  	    check('id').exists().withMessage('Please input contractId')
  	  ];
		}
		
		static contractUpdate() {
			return [
				check('id').exists().withMessage('Please input contractId'),
				check('data').exists().withMessage('Please input data'),
			];
		}
}

module.exports = ContractValidator;