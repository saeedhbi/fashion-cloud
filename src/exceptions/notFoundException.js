const BaseException = require('@app/baseException');

class NotFoundException extends BaseException {
	constructor() {
		super();

		this.status = 404;
		this.message = 'Entity not found';
	}
}

module.exports = NotFoundException;