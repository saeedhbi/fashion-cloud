require('module-alias/register');
const container = require('./app');

const app = container.resolve('app');

app
  .start()
  .catch((error) => {
    process.exit();
  });