class Database {
	constructor(config) {
		this.config = config;
		
		return this;
	}
	
	connect() {
		const mongoose = require('mongoose');
		
		return mongoose.connect(`mongodb://${this.config.host}:${this.config.port}/${this.config.database}`).then(() => {
				console.log('Database connection successful')
			})
			.catch(err => {
				console.error('Database connection error', err)
			});
	}
}

module.exports = Database;