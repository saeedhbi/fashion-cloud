const express = require('express');

class Server {
  constructor({ config, router }) {
    this.config = config;
    this.express = express();

    this.express.use(express.json());
    this.express.use(router);
  }

  start() {
    return new Promise((resolve) => {
      this.express
        .listen(this.config.web.port, () => {
          console.log('App is listening to port ' + this.config.web.port);
          
          resolve();
        });
    });
  }
}

module.exports = Server;