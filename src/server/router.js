const { Router } = require('express');
const bodyParser = require('body-parser');

const contractControllerRoutes = require('@domain/contract/routes');

module.exports = ({ 
	containerMiddleware, 
	contractController 
}) => {

	const router = Router();

	const apiRouter = Router();

	apiRouter
	.use(bodyParser.json())
	.use(containerMiddleware)

	var contractAPI = new contractControllerRoutes(contractController);

	apiRouter.use('/contract', contractAPI.router());

	router.use('/api/v1', apiRouter);

	return router;
};